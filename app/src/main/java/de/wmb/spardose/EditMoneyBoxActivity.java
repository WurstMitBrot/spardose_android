package de.wmb.spardose;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.time.LocalDateTime;

import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;
import de.wmb.spardose.util.Constants;
import de.wmb.spardose.viewmodel.MoneyBoxViewModel;

import static de.wmb.spardose.util.Constants.EXTRA_ACTITIVTY_MODE;
import static de.wmb.spardose.util.Constants.EXTRA_MONEYBOX;


public class EditMoneyBoxActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Constants.ActivityMode activityMode;
    private MoneyBoxViewModel moneyBoxViewModel;
    private MoneyBox moneyBox;

    private EditText editMoneyBoxName;
    private EditText editMoneyBoxDescription;
    private EditText editMoneyBoxAmount;
    private Spinner spinnerTransactionType;

    private boolean isInpayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_money_box);

        editMoneyBoxName = findViewById(R.id.editTransactionDescription);
        editMoneyBoxDescription = findViewById(R.id.editBoxDescription);
        setUpEditAmount();
        spinnerTransactionType = findViewById(R.id.spinTransactionType);
        spinnerTransactionType.setOnItemSelectedListener(this);
        setSpinnerData();

        moneyBoxViewModel = ViewModelProviders.of(this).get(MoneyBoxViewModel.class);

        Intent intent = getIntent();
        if(intent.hasExtra(EXTRA_ACTITIVTY_MODE)){
            this.activityMode = (Constants.ActivityMode) intent.getSerializableExtra(EXTRA_ACTITIVTY_MODE);
        }
        if(activityMode == Constants.ActivityMode.UPDATE){

            if(intent.hasExtra(EXTRA_MONEYBOX)){
                moneyBox = intent.getParcelableExtra(EXTRA_MONEYBOX);
                setMoneyBoxData();
            }
        }
    }

    private void setMoneyBoxData() {
        setTitle(moneyBox.getName() + "*");
        editMoneyBoxName.setText(moneyBox.getName());
        editMoneyBoxDescription.setText(moneyBox.getDescription());

        // hide elements
        editMoneyBoxAmount.setVisibility(View.GONE);
        spinnerTransactionType.setVisibility(View.GONE);

        TextView txtAmount = findViewById(R.id.txtAmount);
        txtAmount.setVisibility(View.GONE);
        TextView txtCurrency = findViewById(R.id.txtCurrency);
        txtCurrency.setVisibility(View.GONE);
        TextView txtTransactionArt = findViewById(R.id.txtTransactionArt);
        txtTransactionArt.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_moneybox, menu);

        // nur ein Menu für beide Activities edit wird hier nicht gebraucht
        MenuItem item = menu.findItem(R.id.itemEdit);
        item.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.itemSave:
                saveMoneyBox();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpEditAmount(){
        editMoneyBoxAmount = findViewById(R.id.editTransactionAmount);
        // TODO find out how to change Decimal Seperator
        char separator = ',';//DecimalFormatSymbols.getInstance().getDecimalSeparator();
        editMoneyBoxAmount.setKeyListener(DigitsKeyListener.getInstance("0123456789" + separator));
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                String amountText = editMoneyBoxAmount.getText().toString();
                boolean hasComma = amountText.contains(",");
                for (int i = start; i < end; i++) {
                    if (source.charAt(i) == ',') {
                        if(hasComma)
                            return "";
                    }
                }
                return null;
            }
        };
        editMoneyBoxAmount.setFilters(new InputFilter[] { filter });
    }

    private void setSpinnerData(){
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.transactionTypeArray, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerTransactionType.setAdapter(adapter);
        spinnerTransactionType.setId(0);
        isInpayment = true;
    }

    public void saveMoneyBox(){
       Transaction startTransaction = null;

        String name = editMoneyBoxName.getText().toString();
        String description = editMoneyBoxDescription.getText().toString();
        String amountText = editMoneyBoxAmount.getText().toString();
        amountText = amountText.replace(',', '.');
        amountText = amountText.isEmpty() ? "0" : amountText;

        // Einzahlung oder Auszahlung
        isInpayment = spinnerTransactionType.getSelectedItem().toString().equals("Einzahlung");
        double amount = Double.parseDouble(amountText);

        if(name.isEmpty())
            return;

        if(moneyBox == null){
            moneyBox = new MoneyBox();
            moneyBox.setAmount(0d);
            moneyBox.setGoal(Double.valueOf(0));
            moneyBox.setDeleted(false);
            moneyBox.setNew(true);
            moneyBox.setGoalDate(LocalDateTime.now());

            // Wenn die Menge != 0 ist dann auch eine Transaction anlegen
            if(amount != 0){
                startTransaction = new Transaction();
                startTransaction.setDescription("Anfangszahlung");
                startTransaction.setAmount(amount);
                startTransaction.setNew(true);
                startTransaction.setDeleted(false);
                startTransaction.setInpayment(isInpayment);
                startTransaction.setPayDate(LocalDateTime.now());
            }
        }

        moneyBox.setName(name);
        moneyBox.setDescription(description);

        if(startTransaction == null){
            if(activityMode == Constants.ActivityMode.NEW)
                moneyBoxViewModel.insert(moneyBox);
            else
                moneyBoxViewModel.update(moneyBox);
        }else{
            moneyBoxViewModel.insert(moneyBox, startTransaction);
        }

        Intent replyIntent = new Intent();
        if(moneyBox == null){
            setResult(RESULT_CANCELED, replyIntent);
        }else{
            setResult(RESULT_OK, replyIntent);
        }
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        isInpayment = id == 0;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        spinnerTransactionType.setId(0);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED, null);
        super.onBackPressed();
    }
}
