package de.wmb.spardose;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;
import de.wmb.spardose.util.Constants;
import de.wmb.spardose.viewmodel.TransactionViewModel;

import static de.wmb.spardose.util.Constants.EXTRA_ACTITIVTY_MODE;
import static de.wmb.spardose.util.Constants.EXTRA_MONEYBOX;
import static de.wmb.spardose.util.Constants.EXTRA_TRANSACTION;

public class TransactionActivity extends AppCompatActivity {

    private TransactionViewModel transactionViewModel;

    private Constants.ActivityMode activityMode;
    private MoneyBox moneyBox;
    private Transaction transaction;

    private EditText editTransactionDescription;
    private EditText editTransactionAmount;
    private Spinner spinnerTransactionType;
    private Menu menu;

    private boolean isInpayment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        // Views holen
        editTransactionDescription = findViewById(R.id.editTransactionDescription);
        setUpEditAmount();
        spinnerTransactionType = findViewById(R.id.spinTransactionType);
        setSpinnerData();
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);

        // MoneyBox holen
        Intent intent = getIntent();
        if(intent.hasExtra(EXTRA_ACTITIVTY_MODE)){
            this.activityMode = (Constants.ActivityMode) intent.getSerializableExtra(EXTRA_ACTITIVTY_MODE);
        }

        if(intent.hasExtra(EXTRA_MONEYBOX)){
            moneyBox = intent.getParcelableExtra(EXTRA_MONEYBOX);
        }

        if(activityMode != Constants.ActivityMode.NEW){
            setTransactionData();
            setTitle(moneyBox.getName());
        }else{
            setTitle("Neue Transaktion");
        }

        if(activityMode == Constants.ActivityMode.READ)
            setEditMode(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_transaction, menu);
        this.menu = menu;

        if(activityMode == Constants.ActivityMode.READ){
            MenuItem item = menu.findItem(R.id.itemSave);
            item.setVisible(false);
        }else{
            MenuItem item = menu.findItem(R.id.itemEdit);
            item.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.itemEdit:
                setEditMode(true);
                return true;
            case R.id.itemSave:
                saveTransaction(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setEditMode(boolean isEdit) {
        editTransactionAmount.setEnabled(isEdit);
        editTransactionDescription.setEnabled(isEdit);
        spinnerTransactionType.setEnabled(isEdit);

        if(menu != null) {
            MenuItem itemSave = menu.findItem(R.id.itemSave);
            itemSave.setVisible(isEdit);
            MenuItem itemEdit = menu.findItem(R.id.itemEdit);
            itemEdit.setVisible(!isEdit);
        }
    }

    private void setTransactionData() {
        // Transaction holen und Daten in die Views schreiben
        Intent intent = getIntent();
        if(intent.hasExtra(EXTRA_TRANSACTION)){
            transaction = intent.getParcelableExtra(EXTRA_TRANSACTION);
            editTransactionDescription.setText(transaction.getDescription());
            editTransactionAmount.setText(transaction.getAmount().toString());
            isInpayment = transaction.getAmount() < 0 ? false : true;
            if(isInpayment)
                spinnerTransactionType.setSelection(0);
            else
                spinnerTransactionType.setSelection(1);
        }
    }

    private void setUpEditAmount(){
        editTransactionAmount = findViewById(R.id.editTransactionAmount);
        // TODO find out how to change Decimal Seperator
        char separator = ',';//DecimalFormatSymbols.getInstance().getDecimalSeparator();
        editTransactionAmount.setKeyListener(DigitsKeyListener.getInstance("0123456789" + separator));
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                String amountText = editTransactionAmount.getText().toString();
                boolean hasComma = amountText.contains(",");
                for (int i = start; i < end; i++) {
                    if (source.charAt(i) == ',') {
                        if(hasComma)
                            return "";
                    }
                }
                return null;
            }
        };
        editTransactionAmount.setFilters(new InputFilter[] { filter });
    }

    private void setSpinnerData(){
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.transactionTypeArray, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerTransactionType.setAdapter(adapter);
        spinnerTransactionType.setId(0);
        isInpayment = true;
    }

    public void saveTransaction(View view){
        String description = editTransactionDescription.getText().toString();
        String amountText = editTransactionAmount.getText().toString();
        amountText = amountText.isEmpty() ? "0" : amountText;
        amountText = amountText.replace(',', '.');

        double amount = Double.parseDouble(amountText);
        // Einzahlung oder Auszahlung
        isInpayment = spinnerTransactionType.getSelectedItem().toString().equals("Einzahlung");

        if (description.isEmpty() || amount == 0){
            // GIVE TOAST
            Toast.makeText(this, "Die Beschreibung sollte nicht leer und der Betrag sollte nicht 0 sein", Toast.LENGTH_LONG);
            return;
        }

        if(transaction == null){
            transaction = new Transaction();
            transaction.setDescription(description);
            transaction.setMoneyBoxID(moneyBox.getId());
            transaction.setAmount(amount);
            transaction.setDeleted(false);
            transaction.setNew(true);
            transaction.setInpayment(isInpayment);

            LocalDateTime time = LocalDateTime.now();
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            dateTimeFormatter.format(time);
            transaction.setPayDate(time);

            transactionViewModel.insert(transaction, moneyBox);
        }else{
            transaction.setDescription(description);
            transaction.setAmount(amount);
            transaction.setInpayment(isInpayment);

            transactionViewModel.updateTransaction(transaction, moneyBox);
        }

        Intent replyIntent = new Intent();
        if(transaction == null){
            setResult(RESULT_CANCELED, replyIntent);
        }else{
            setResult(RESULT_OK, replyIntent);
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED, null);
        super.onBackPressed();
    }
}
