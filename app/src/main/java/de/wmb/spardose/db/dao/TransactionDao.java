package de.wmb.spardose.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import de.wmb.spardose.db.entity.Transaction;

@Dao
public interface TransactionDao {

    @Insert
    void insertTransaction(Transaction transaction);

    // ORDER BY payDate DESC
    @Query("SELECT * from transaction_table ORDER BY payDate DESC")
    LiveData<List<Transaction>> getAllTransactions();

    @Query("SELECT * from transaction_table WHERE moneyBoxId = :moneyBoxId ORDER BY payDate DESC")
    LiveData<List<Transaction>> getTransactionsByMoneyBoxId(Long moneyBoxId);

    @Query("SELECT * from transaction_table WHERE moneyBoxId = :moneyBoxId ORDER BY sortPosition")
    LiveData<List<Transaction>> getTransactionsByMoneyBoxIdCustomSort(Long moneyBoxId);

    @Query("SELECT SUM(amount) from transaction_table WHERE moneyBoxId = :moneyBoxId")
    Double getTransactionSumByMoneyBoxId(Long moneyBoxId);

    @Query("DELETE FROM transaction_table WHERE id = :transactionId")
    void deleteTransactionById(Long transactionId);

    @Query("SELECT MAX(sortPosition) FROM transaction_table")
    Integer getLastSortPosition();

    @Query("DELETE FROM transaction_table")
    void deleteAllTransactions();

    @Update
    void updateTransaction(Transaction transaction);

    @Update
    void updateTransactions(List<Transaction> transactions);
}
