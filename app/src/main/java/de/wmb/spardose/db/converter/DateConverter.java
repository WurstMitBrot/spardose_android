package de.wmb.spardose.db.converter;

import android.arch.persistence.room.TypeConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateConverter {
    @TypeConverter
    public static LocalDateTime toDate(Long timestamp){
        LocalDateTime ldt;
        if(timestamp == null){
            return null;
        }else{
            ldt = Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        return ldt;
    }

    @TypeConverter
    public static Long toTimestamp(LocalDateTime date){
        if(date == null){
            return null;
        }else{
            return date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        }
    }
}
