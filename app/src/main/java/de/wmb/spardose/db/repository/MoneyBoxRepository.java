package de.wmb.spardose.db.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import de.wmb.spardose.db.MoneyBoxRoomDatabase;
import de.wmb.spardose.db.dao.MoneyBoxDao;
import de.wmb.spardose.db.entity.MoneyBox;

public class MoneyBoxRepository {

    private MoneyBoxDao moneyBoxDao;
    private LiveData<List<MoneyBox>> allMoneyBoxes;

    public MoneyBoxRepository(Application application){
        MoneyBoxRoomDatabase db = MoneyBoxRoomDatabase.getDatabase(application);
        moneyBoxDao = db.moneyBoxDao();
        allMoneyBoxes = moneyBoxDao.getAllMoneyBoxes();
    }

    public LiveData<List<MoneyBox>> getAllMoneyBoxes(){
        return allMoneyBoxes;
    }

    // get MoneyBox By Id Call
    public MoneyBox getById(Long id) {
        MoneyBox moneyBox = null;
        try {
            moneyBox = new getMoneyBoxByIdAsyncTask(moneyBoxDao).execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return moneyBox;
    }

    // get MoneyBox By Id AsyncTask
    private static class getMoneyBoxByIdAsyncTask extends AsyncTask<Long, Void, MoneyBox> {

        private MoneyBoxDao asyncTaskDao;

        getMoneyBoxByIdAsyncTask(MoneyBoxDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected MoneyBox doInBackground(final Long... params) {
            return asyncTaskDao.getMoneyBoxById(params[0]);
        }
    }

    // Insert MoneyBox Call
    public Long insert (MoneyBox moneyBox) throws ExecutionException, InterruptedException {
        return new insertAsyncTask(moneyBoxDao).execute(moneyBox).get();
    }

    // Insert MoneyBox AsyncTask
    private static class insertAsyncTask extends AsyncTask<MoneyBox, Void, Long> {

        private MoneyBoxDao asyncTaskDao;

        insertAsyncTask(MoneyBoxDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Long doInBackground(final MoneyBox... params) {
            Integer lastSortPosition = asyncTaskDao.getLastSortPosition();
            lastSortPosition = lastSortPosition == null ? 1 : ++lastSortPosition;

            MoneyBox moneyBox = params[0];
            moneyBox.setSortPosition(lastSortPosition);
            return asyncTaskDao.insertMoneyBox(moneyBox);
        }
    }

    // Update MoneyBox Call
    public void update(MoneyBox moneyBox){
        new updateAsyncTask(moneyBoxDao).execute(moneyBox);
    }

    // Update MoneyBox AsyncTask
    private static class updateAsyncTask extends AsyncTask<MoneyBox, Void, Void> {

        private MoneyBoxDao asyncTaskDao;

        updateAsyncTask(MoneyBoxDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoneyBox... params) {
            MoneyBox moneyBox = params[0];
            asyncTaskDao.updateMoneyBox(moneyBox);
            return null;
        }
    }

    // Update MoneyBox Amount Call
    public void updateAmount(MoneyBox moneyBox, Double amount){
        moneyBox.setAmount(amount);
        new updateAmountAsyncTask(moneyBoxDao).execute(moneyBox);
    }

    // Update MoneyBox Amount AsyncTask
    private static class updateAmountAsyncTask extends AsyncTask<MoneyBox, Void, Void> {

        private MoneyBoxDao asyncTaskDao;

        updateAmountAsyncTask(MoneyBoxDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoneyBox... params) {
            asyncTaskDao.updateMoneyBoxAmount(params[0].getId(), params[0].getAmount());
            return null;
        }
    }

    // Delete MoneyBox by id Call
    public void deleteMoneyBoxById (MoneyBox moneyBox) {
        new deleteMoneyBoxByIdAsyncTask(moneyBoxDao).execute(moneyBox);
    }

    // Delete MoneyBox by id  AsyncTask
    private static class deleteMoneyBoxByIdAsyncTask extends AsyncTask<MoneyBox, Void, Void> {
        private MoneyBoxDao asyncTaskDao;

        deleteMoneyBoxByIdAsyncTask(MoneyBoxDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(MoneyBox... boxes) {
            asyncTaskDao.deleteMoneyBoxById(boxes[0].getId());
            return null;
        }
    }

    // Delete all MoneyBoxes Call
    public void deleteAll () {
        new deleteAllMoneyBoxesAsyncTask(moneyBoxDao).execute();
    }

    // Delete all MoneyBoxes AsyncTask
    private static class deleteAllMoneyBoxesAsyncTask extends AsyncTask<Void, Void, Void> {
        private MoneyBoxDao asyncTaskDao;

        deleteAllMoneyBoxesAsyncTask(MoneyBoxDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            asyncTaskDao.deleteAllMoneyBoxes();
            return null;
        }
    }

    // Custom sort MoneyBoxes Call
    public void sortCustomMoneyBoxes(List<MoneyBox> moneyBoxes){
        // Update all MoneyBox Objects below the updated MoneyBox

        MoneyBox[] boxes = new MoneyBox[moneyBoxes.size()];
        boxes = moneyBoxes.toArray(boxes);
        new sortCustomMoneyBoxesAsyncCall(moneyBoxDao).execute(boxes);
    }

    // Update MoneyBox Amount AsyncTask
    private static class sortCustomMoneyBoxesAsyncCall extends AsyncTask<MoneyBox, Void, Void> {

        private MoneyBoxDao asyncTaskDao;

        sortCustomMoneyBoxesAsyncCall(MoneyBoxDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoneyBox... moneyBoxes) {
            asyncTaskDao.updateMoneyBoxes(Arrays.asList(moneyBoxes));
            return null;
        }
    }

}
