package de.wmb.spardose.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.time.LocalDateTime;

import de.wmb.spardose.db.converter.DateConverter;
import lombok.Getter;
import lombok.Setter;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Getter
@Setter
@TypeConverters(DateConverter.class)

@Entity(tableName = "transaction_table",
        foreignKeys = @ForeignKey(onDelete = CASCADE,
                entity = MoneyBox.class,
                parentColumns = "id",
                childColumns = "moneyBoxId"),
        indices = {@Index("moneyBoxId"),
                @Index(value = {"id", "moneyBoxId"})})

public class Transaction implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Long id;
    @NonNull
    @ColumnInfo(name = "moneyBoxId")
    private Long moneyBoxID;
    private String description;
    @NonNull
    private Double amount;
    @ColumnInfo(name = "payDate")
    private LocalDateTime payDate;
    @NonNull
    private boolean isInpayment;
    private boolean isDeleted;
    private boolean isNew;
    @NonNull
    private Integer sortPosition;

    public Transaction(){}

    protected Transaction(Parcel in) {
        String dateString = "";

        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        if (in.readByte() == 0) {
            moneyBoxID = null;
        } else {
            moneyBoxID = in.readLong();
        }
        description = in.readString();
        amount = in.readDouble();

        dateString = in.readString();
        payDate = LocalDateTime.parse(dateString);

        isInpayment = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
        isNew = in.readByte() != 0;
        sortPosition = in.readInt();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        if (moneyBoxID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(moneyBoxID);
        }
        dest.writeString(description);
        dest.writeDouble(amount);
        dest.writeString(payDate.toString());
        dest.writeByte((byte) (isInpayment ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeByte((byte) (isNew ? 1 : 0));
        dest.writeInt(sortPosition);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", moneyBoxID=" + moneyBoxID +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", payDate=" + payDate +
                ", isInpayment=" + isInpayment +
                ", isDeleted=" + isDeleted +
                ", isNew=" + isNew +
                ", sortPosition=" + sortPosition +
                '}';
    }
}