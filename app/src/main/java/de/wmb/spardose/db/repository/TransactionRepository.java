package de.wmb.spardose.db.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import de.wmb.spardose.db.MoneyBoxRoomDatabase;
import de.wmb.spardose.db.dao.TransactionDao;
import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;

public class TransactionRepository {

    private TransactionDao transactionDao;
    private LiveData<List<Transaction>> allTransactions;

    public TransactionRepository(Application application){
        MoneyBoxRoomDatabase db = MoneyBoxRoomDatabase.getDatabase(application);
        transactionDao = db.transactionDao();
        allTransactions = transactionDao.getAllTransactions();
    }

    public LiveData<List<Transaction>> getAllTransactions(){
        return allTransactions;
    }

    public LiveData<List<Transaction>> getTransactionsByMoneyBoxId(MoneyBox moneyBox){
        return transactionDao.getTransactionsByMoneyBoxId(moneyBox.getId());
    }

    // Insert Transaction Call
    public void insert (Transaction transaction){
        Double amount = transaction.isInpayment() ? transaction.getAmount() : transaction.getAmount() * -1f;

        transaction.setAmount(amount);
        new insertAsyncTask(transactionDao).execute(transaction);
    }

    // Insert Transaction AsyncTask
    private static class insertAsyncTask extends AsyncTask<Transaction, Void, Void> {

        private TransactionDao asyncTaskDao;

        insertAsyncTask(TransactionDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Transaction... transactions) {
            Transaction newTransaction = transactions[0];
            Integer lastSortPosition = asyncTaskDao.getLastSortPosition();
            lastSortPosition = lastSortPosition == null ? 1 : ++lastSortPosition;
            newTransaction.setSortPosition(lastSortPosition);

            asyncTaskDao.insertTransaction(newTransaction);
            return null;
        }
    }

    // get amount By moneyBoxId Call
    public Double getAmountByMoneyBoxId (MoneyBox moneyBox){
        Double amount = null;
        try {
            amount = new getAmountByMoneyBoxIdAsyncTask(transactionDao).execute(moneyBox).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        amount = amount == null ? 0d : amount;
        return amount;
    }

    // I get amount By moneyBoxId AsyncTask
    private static class getAmountByMoneyBoxIdAsyncTask extends AsyncTask<MoneyBox, Void, Double> {

        private TransactionDao asyncTaskDao;

        getAmountByMoneyBoxIdAsyncTask(TransactionDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Double doInBackground(final MoneyBox... params) {
            return asyncTaskDao.getTransactionSumByMoneyBoxId(params[0].getId());
        }
    }

    // Delete Transaction by id Call
    public void deleteMoneyBoxById (Transaction transaction) {
        new deleteMoneyBoxByIdAsyncTask(transactionDao).execute(transaction);
    }

    // Delete Transaction by id Call
    private static class deleteMoneyBoxByIdAsyncTask extends AsyncTask<Transaction, Void, Void> {
        private TransactionDao asyncTaskDao;

        deleteMoneyBoxByIdAsyncTask(TransactionDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Transaction... transactions) {
            asyncTaskDao.deleteTransactionById(transactions[0].getId());
            return null;
        }
    }

    // Delete all Transactions Call
    public void deleteAllTransactions(){
        new deleteAllTransactionsAsyncTask(transactionDao).execute();
    }

    // Delete all Transactions AsyncTask
    private static class deleteAllTransactionsAsyncTask extends AsyncTask<Void, Void, Void> {

        private TransactionDao asyncTaskDao;

        deleteAllTransactionsAsyncTask(TransactionDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            asyncTaskDao.deleteAllTransactions();
            return null;
        }
    }

    // Sort Transactions and Update Call
    public void sortCustomTransactions(List<Transaction> transactions){
        // Update all MoneyBox Objects below the updated MoneyBox

        Transaction[] transactionsArray = new Transaction[transactions.size()];
        transactionsArray = transactions.toArray(transactionsArray);
        new TransactionRepository.sortCustomTransactionAsyncCall(transactionDao).execute(transactionsArray);
    }

    // Sort Transactions and Update AsyncTask
    private static class sortCustomTransactionAsyncCall extends AsyncTask<Transaction, Void, Void> {

        private TransactionDao asyncTaskDao;

        sortCustomTransactionAsyncCall(TransactionDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Transaction... transactions) {
            asyncTaskDao.updateTransactions(Arrays.asList(transactions));
            return null;
        }
    }

    // Update Transactions Call
    public void updateTransaction(Transaction transaction){
        new updateTransactionAsyncTask(transactionDao).execute(transaction);
    }

    // Update Transactions AsyncTask
    private static class updateTransactionAsyncTask extends AsyncTask<Transaction, Void, Void> {

        private TransactionDao asyncTaskDao;

        updateTransactionAsyncTask(TransactionDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Transaction... transactions) {
            asyncTaskDao.updateTransaction(transactions[0]);
            return null;
        }
    }
}
