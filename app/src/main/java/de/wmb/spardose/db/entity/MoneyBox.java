package de.wmb.spardose.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.time.LocalDateTime;

import de.wmb.spardose.db.converter.DateConverter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TypeConverters(DateConverter.class)
@Entity(tableName = "moneybox_table")
public class MoneyBox implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Long id;
    @ColumnInfo(name = "name")
    private String name;
    private String description;
    private Double amount;
    private Double goal;
    private LocalDateTime goalDate;
    private boolean isDeleted;
    private boolean isNew;
    @NonNull
    private Integer sortPosition;

    protected MoneyBox(Parcel in) {
        String dateString;

        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        name = in.readString();
        description = in.readString();
        amount = in.readDouble();
        goal = in.readDouble();
        dateString = in.readString();
        goalDate = LocalDateTime.parse(dateString);
        isDeleted = in.readByte() != 0;
        isNew = in.readByte() != 0;
        sortPosition = in.readInt();
    }

    public MoneyBox(){    }

    public static final Creator<MoneyBox> CREATOR = new Creator<MoneyBox>() {
        @Override
        public MoneyBox createFromParcel(Parcel in) {
            return new MoneyBox(in);
        }

        @Override
        public MoneyBox[] newArray(int size) {
            return new MoneyBox[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(amount);
        dest.writeDouble(goal);
        dest.writeString(goalDate.toString());
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeByte((byte) (isNew ? 1 : 0));
        dest.writeInt(sortPosition);
    }

    @Override
    public String toString() {
        return "MoneyBox{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", goal=" + goal +
                ", goalDate=" + goalDate +
                ", isDeleted=" + isDeleted +
                ", isNew=" + isNew +
                ", sortPosition=" + sortPosition +
                '}';
    }
}