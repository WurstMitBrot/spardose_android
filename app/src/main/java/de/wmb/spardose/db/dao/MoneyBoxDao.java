package de.wmb.spardose.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import de.wmb.spardose.db.entity.MoneyBox;

@Dao
public interface MoneyBoxDao {

    @Insert
    Long insertMoneyBox(MoneyBox moneyBox);

    @Query("SELECT * from moneybox_table ORDER BY name COLLATE NOCASE ASC")
    LiveData<List<MoneyBox>> getAllMoneyBoxes();

    @Query("SELECT * FROM moneybox_table ORDER BY sortPosition")
    LiveData<List<MoneyBox>> getAllMoneyBoxesCustomSort();

    @Query("SELECT * from moneybox_table WHERE id = :id")
    MoneyBox getMoneyBoxById(Long id);

    @Query("UPDATE moneybox_table SET amount = :amount WHERE id = :id")
    void updateMoneyBoxAmount(long id, double amount);

    @Query("DELETE FROM moneybox_table WHERE id = :moneyBoxId")
    void deleteMoneyBoxById(Long moneyBoxId);

    @Query("DELETE FROM moneybox_table")
    void deleteAllMoneyBoxes();

    @Query("SELECT MAX(sortPosition) FROM moneybox_table")
    Integer getLastSortPosition();

    @Update
    void updateMoneyBox(MoneyBox moneyBox);

    @Update
    void updateMoneyBoxes(List<MoneyBox> moneyBoxes);
}
