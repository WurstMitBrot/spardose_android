package de.wmb.spardose.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import de.wmb.spardose.db.converter.DateConverter;
import de.wmb.spardose.db.dao.MoneyBoxDao;
import de.wmb.spardose.db.dao.TransactionDao;
import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;

@Database(entities = {MoneyBox.class, Transaction.class}, version = 2)
@TypeConverters({DateConverter.class})
public abstract class MoneyBoxRoomDatabase extends RoomDatabase {

    public abstract MoneyBoxDao moneyBoxDao();

    public abstract TransactionDao transactionDao();

    private static volatile MoneyBoxRoomDatabase INSTANCE;

    public static MoneyBoxRoomDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (MoneyBoxRoomDatabase.class){
                if(INSTANCE == null){
                    // create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MoneyBoxRoomDatabase.class, "moneybox_database")
                            .addMigrations(MIGRATION_1_2)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Migrate from:
     * version 1 - using the SQLiteDatabase API
     * to
     * version 2 - using Room
     */
    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE moneybox_table "
                    + " ADD COLUMN sortPosition INTEGER default 0 NOT NULL");

            database.execSQL("ALTER TABLE transaction_table "
                    + " ADD COLUMN sortPosition INTEGER default 0 NOT NULL");
        }
    };
}
