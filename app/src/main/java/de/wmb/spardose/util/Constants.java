package de.wmb.spardose.util;

public class Constants {

    public static final String EXTRA_ACTITIVTY_MODE = "ActivityMode";
    public final static String EXTRA_MONEYBOX = "MoneyBox";
    public final static String EXTRA_NEW_MONEYBOX = "newMoneyBox";
    public final static String EXTRA_DELETE_MONEYBOX = "deleteMoneyBox";
    public final static String EXTRA_TRANSACTION = "Transaction";
    public final static String EXTRA_NEW_TRANSACTION = "newTransaction";

    public final static int INTENT_CODE_NEW_MONEYBOX = 1;
    public final static int INTENT_CODE_MONEYBOX = 2;
    public final static int INTENT_CODE_NEW_TRANSACTION = 3;
    public final static int INTENT_CODE_TRANSACTION = 4;
    public final static int INTENT_CODE_EDIT_MONEYBOX = 5;

    public static final int SORT_TYPE_DEFAULT = 0;
    public static final int SORT_TYPE_NAME = 1;
    public static final int SORT_TYPE_CUSTOM = 2;

    public enum ActivityMode{
        READ, UPDATE, NEW;
    }
}
