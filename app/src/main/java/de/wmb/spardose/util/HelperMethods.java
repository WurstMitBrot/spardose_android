package de.wmb.spardose.util;

import java.text.DecimalFormat;

public class HelperMethods {
    public static String getFormattedAmountString(Double amount){
        DecimalFormat df = new DecimalFormat("#.00");

        String amountText = amount != 0 ? df.format(amount) : "0.00";
        amountText = amountText.replace('.', ',');
        amountText += " €";

        return amountText;
    }
}
