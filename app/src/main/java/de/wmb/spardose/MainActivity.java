package de.wmb.spardose;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.util.Constants;
import de.wmb.spardose.viewmodel.MoneyBoxItemCallback;
import de.wmb.spardose.viewmodel.MoneyBoxListAdapter;
import de.wmb.spardose.viewmodel.MoneyBoxViewModel;

import static de.wmb.spardose.util.Constants.EXTRA_ACTITIVTY_MODE;
import static de.wmb.spardose.util.Constants.EXTRA_DELETE_MONEYBOX;
import static de.wmb.spardose.util.Constants.EXTRA_MONEYBOX;
import static de.wmb.spardose.util.Constants.INTENT_CODE_MONEYBOX;
import static de.wmb.spardose.util.Constants.INTENT_CODE_NEW_MONEYBOX;
import static de.wmb.spardose.util.Constants.SORT_TYPE_CUSTOM;

public class MainActivity extends AppCompatActivity implements MoneyBoxItemCallback.ItemTouchHelperMoneyBoxActivity {
    private MoneyBoxViewModel moneyBoxViewModel;
    private MoneyBoxListAdapter moneyBoxListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            openNewMoneyBoxActivity();
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        moneyBoxListAdapter = new MoneyBoxListAdapter(this);
        recyclerView.setAdapter(moneyBoxListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // add swipe and move to recycler view
        ItemTouchHelper.SimpleCallback callback = new MoneyBoxItemCallback(
                this,
                moneyBoxListAdapter,
                ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT);

        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        // set RecyclerView on item click listener
        moneyBoxListAdapter.setOnItemListener(new MoneyBoxListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MoneyBox moneyBox) {
                openMoneyBoxActivity(moneyBox, false);
            }
        });

        moneyBoxViewModel = ViewModelProviders.of(this).get(MoneyBoxViewModel.class);

        moneyBoxViewModel.getAllMoneyBoxes().observe(this, moneyBoxes -> moneyBoxListAdapter.setMoneyBoxes(moneyBoxes, SORT_TYPE_CUSTOM));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openNewMoneyBoxActivity(){
        Intent intent = new Intent(this, EditMoneyBoxActivity.class);
        intent.putExtra(EXTRA_ACTITIVTY_MODE, Constants.ActivityMode.NEW);
        startActivityForResult(intent, INTENT_CODE_NEW_MONEYBOX);
    }

    public void openMoneyBoxActivity(MoneyBox moneyBox, boolean isEdit){
        if(isEdit){
            Intent intent = new Intent(this, EditMoneyBoxActivity.class);
            intent.putExtra(EXTRA_MONEYBOX,moneyBox);
            intent.putExtra(EXTRA_ACTITIVTY_MODE, Constants.ActivityMode.UPDATE);
            startActivityForResult(intent, INTENT_CODE_MONEYBOX);
        }else{
            Intent intent = new Intent(this, MoneyBoxActivity.class);
            intent.putExtra(EXTRA_MONEYBOX,moneyBox);
            startActivityForResult(intent, INTENT_CODE_MONEYBOX);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED)
            return;

        switch(requestCode){
            case INTENT_CODE_MONEYBOX:
                handleMoneyBoxResult(resultCode, data);
                break;
        }
    }

    private void handleMoneyBoxResult(int resultCode, Intent data){
        MoneyBox moneyBox;
        if(data.hasExtra(EXTRA_DELETE_MONEYBOX) && resultCode == RESULT_OK){
            moneyBox = data.getParcelableExtra(EXTRA_DELETE_MONEYBOX);
            if(moneyBox != null){
                deleteMoneyBox(moneyBox);
                Toast.makeText(this, "Spardose gelöscht.", Toast.LENGTH_LONG);
            }
        }
    }

    private void deleteMoneyBox(final MoneyBox moneyBox) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Soll die Spardose '" + moneyBox.getName() + "' wirklich gelöscht werden?")
                .setCancelable(false)
                .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        moneyBoxViewModel.deleteMoneyBoxById(moneyBox);
                    }
                })
                .setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        moneyBoxListAdapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemDeleteSwipe(MoneyBox moneyBox) {
        deleteMoneyBox(moneyBox);
    }

    @Override
    public void onItemEditSwipe(MoneyBox moneyBox) {
        moneyBoxListAdapter.notifyDataSetChanged();
       openMoneyBoxActivity(moneyBox, true);
    }

    @Override
    public void onListSortChange(List<MoneyBox> moneyBoxes) {
        moneyBoxViewModel.updateMoneyBoxSort(moneyBoxes);
    }
}
