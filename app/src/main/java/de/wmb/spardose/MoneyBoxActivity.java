package de.wmb.spardose;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;
import de.wmb.spardose.util.Constants;
import de.wmb.spardose.viewmodel.TransactionItemCallback;
import de.wmb.spardose.viewmodel.TransactionListAdapter;
import de.wmb.spardose.viewmodel.TransactionViewModel;

import static de.wmb.spardose.util.Constants.EXTRA_ACTITIVTY_MODE;
import static de.wmb.spardose.util.Constants.EXTRA_MONEYBOX;
import static de.wmb.spardose.util.Constants.EXTRA_TRANSACTION;
import static de.wmb.spardose.util.Constants.INTENT_CODE_MONEYBOX;
import static de.wmb.spardose.util.Constants.INTENT_CODE_NEW_TRANSACTION;
import static de.wmb.spardose.util.Constants.INTENT_CODE_TRANSACTION;
import static de.wmb.spardose.util.Constants.SORT_TYPE_DEFAULT;
import static de.wmb.spardose.util.HelperMethods.getFormattedAmountString;

public class MoneyBoxActivity extends AppCompatActivity implements TransactionItemCallback.ItemTouchHelperTransactionActivity {

    private MoneyBox moneyBox;

    private TextView txtDescription;
    private TextView txtAmount;

    private RecyclerView recyclerView;
    private TransactionViewModel transactionViewModel;
    private TransactionListAdapter transactionListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_box);

        // Views laden
        txtDescription = findViewById(R.id.txtDescription);
        txtAmount = findViewById(R.id.txtAmount);
        recyclerView = findViewById(R.id.recyclerViewTransactions);

        // MoneyBox aus den Intent Daten holen
        Intent intent = getIntent();
        moneyBox = intent.getParcelableExtra(EXTRA_MONEYBOX);

        // Views mit Daten befüllen
        if(moneyBox != null){
            setTitle(moneyBox.getName());
            txtDescription.setText(moneyBox.getDescription());

            txtAmount.setText(getFormattedAmountString(moneyBox.getAmount()));
        }

        // recyclerView laden
        transactionListAdapter = new TransactionListAdapter(this);
        recyclerView.setAdapter(transactionListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper.SimpleCallback callback = new TransactionItemCallback(
                this,
                transactionListAdapter,
                0,
                ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT);

        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);


        // set RecyclerView on item click listner
        final Context context = this;
        transactionListAdapter.setOnItemListener(new TransactionListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Transaction transaction) {
                openTransactionActivity(transaction, false);
            }
        });

        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getTransactionByMoneyBoxId(moneyBox).observe(this, new Observer<List<Transaction>>(){

            @Override
            public void onChanged(@Nullable List<Transaction> transactions) {
                transactionListAdapter.setTransactions(transactions, SORT_TYPE_DEFAULT);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_moneybox, menu);

        // nur ein Menu für beide Activities edit wird hier nicht gebraucht
        MenuItem item = menu.findItem(R.id.itemSave);
        item.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.itemEdit:
                openEditMoneyBox();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openEditMoneyBox() {
        Intent intent = new Intent(this, EditMoneyBoxActivity.class);
        intent.putExtra(EXTRA_MONEYBOX,moneyBox);
        intent.putExtra(EXTRA_ACTITIVTY_MODE, Constants.ActivityMode.UPDATE);
        startActivityForResult(intent, INTENT_CODE_MONEYBOX);
    }

    public void onNewButtonClick(View view){
        openNewTransactionActivity();
    }

    public void openNewTransactionActivity(){
        Intent intent = new Intent(this, TransactionActivity.class);
        intent.putExtra(EXTRA_ACTITIVTY_MODE, Constants.ActivityMode.NEW);
        intent.putExtra(EXTRA_MONEYBOX, moneyBox);
        startActivityForResult(intent, INTENT_CODE_NEW_TRANSACTION);
    }

    public void openTransactionActivity(Transaction transaction, boolean isEdit){
        Intent intent = new Intent(this, TransactionActivity.class);
        if(isEdit)
            intent.putExtra(EXTRA_ACTITIVTY_MODE, Constants.ActivityMode.UPDATE);
        else
            intent.putExtra(EXTRA_ACTITIVTY_MODE, Constants.ActivityMode.READ);

        intent.putExtra(EXTRA_MONEYBOX, moneyBox);
        intent.putExtra(EXTRA_TRANSACTION, transaction);
        startActivityForResult(intent, INTENT_CODE_TRANSACTION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED)
            return;

        updateMoneyBox();
    }

    private void updateMoneyBox(){
        moneyBox = transactionViewModel.getMoneyBoxById(moneyBox.getId());

        setTitle(moneyBox.getName());
        txtDescription.setText(moneyBox.getDescription());
        txtAmount.setText(getFormattedAmountString(moneyBox.getAmount()));
    }

    private void deleteTransaction(final Transaction transaction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Soll der Eintrag '" + transaction.getDescription() + "' wirklich gelöscht werden?")
                .setCancelable(false)
                .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        transactionViewModel.deleteTransactionById(transaction);
                        updateMoneyBox();
                    }
                })
                .setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        transactionListAdapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // interface implementation --> Callbacks from ItemTouchHelper class
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemDeleteSwipe(Transaction transaction) {
        deleteTransaction(transaction);
    }

    @Override
    public void onItemEditSwipe(Transaction transaction) {
        transactionListAdapter.notifyDataSetChanged();
        openTransactionActivity(transaction, true);
    }

    @Override
    public void onListSortChange(List<Transaction> transactions) {
        transactionViewModel.updateTransactionSort(transactions);
    }
}
