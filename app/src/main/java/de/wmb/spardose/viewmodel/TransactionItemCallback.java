package de.wmb.spardose.viewmodel;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import java.util.Collections;
import java.util.List;

import de.wmb.spardose.R;
import de.wmb.spardose.db.entity.Transaction;

public class TransactionItemCallback extends ItemTouchHelper.SimpleCallback {

    private ItemTouchHelperTransactionActivity activity;
    private final ItemTouchHelperTransactionAdapter adapter;

    private boolean hasMoved = false;
    private List<Transaction> transactions = null;
    private int startPosition = -1;
    private int endPosition = -1;

    public TransactionItemCallback(ItemTouchHelperTransactionActivity activity, ItemTouchHelperTransactionAdapter adapter, int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);
        this.activity = activity;
        this.adapter = adapter;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        // variable to swap the element while moving
        Integer fromPos = viewHolder.getAdapterPosition();
        endPosition = target.getAdapterPosition();

        // only save from pos during the first call of onMove
        if(!hasMoved){
            hasMoved = true;
            startPosition = fromPos;
            transactions = adapter.getTransactions();
        }

        // move item in recyclerview and swap in list
        adapter.onRowMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        Collections.swap(transactions, fromPos, endPosition);

        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder,
                                  int actionState) {
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            // TODO warum nicht aufs Inteface abfragen?
            // change background to gray
            if (viewHolder instanceof TransactionListAdapter.TransactionViewHolder) {
                TransactionListAdapter.TransactionViewHolder transactionViewHolder =
                        (TransactionListAdapter.TransactionViewHolder) viewHolder;
                adapter.onRowSelected(transactionViewHolder);
            }
        }
        super.onSelectedChanged(viewHolder, actionState);
    }

    @Override
    public void clearView(RecyclerView recyclerView,
                          RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);

        if(hasMoved && startPosition != endPosition){
            // write swapped list to database
            activity.onListSortChange(transactions);

            hasMoved = false;
            startPosition = -1;
            endPosition = -1;
        }

        // TODO warum nicht aufs Inteface abfragen?
        // turn background back to normal
        if (viewHolder instanceof TransactionListAdapter.TransactionViewHolder) {
            TransactionListAdapter.TransactionViewHolder transactionViewHolder =
                    (TransactionListAdapter.TransactionViewHolder) viewHolder;
            adapter.onRowClear(transactionViewHolder);
        }
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        Transaction transaction = adapter.getTransactionAt(viewHolder.getAdapterPosition());
        if (i == ItemTouchHelper.LEFT) {
            activity.onItemDeleteSwipe(transaction);
        } else if (i == ItemTouchHelper.RIGHT) {
            activity.onItemEditSwipe(transaction);
        }
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

        dX = dX/ 2;
        Drawable icon = activity.getContext().getResources().getDrawable(R.drawable.ic_edit_white_24dp, activity.getContext().getTheme());
        ColorDrawable background;
        background = new ColorDrawable(Color.GREEN);

        View itemView = viewHolder.itemView;
        int backgroundCornerOffset = 500; //so background is behind the rounded corners of itemView

        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 4;
        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
        int iconBottom = iconTop + icon.getIntrinsicHeight();

        if (dX > 0) { // Swiping to the right
            icon = activity.getContext().getResources().getDrawable(R.drawable.ic_edit_white_24dp, activity.getContext().getTheme());
            // TODO get color from colors.xml
            background = new ColorDrawable(Color.BLUE);
            int iconRight = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
            int iconLeft = itemView.getLeft() + iconMargin;
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
            background.setBounds(itemView.getLeft(), itemView.getTop(),
                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
        } else if (dX < 0) { // Swiping to the left
            icon = activity.getContext().getResources().getDrawable(R.drawable.ic_delete_white, activity.getContext().getTheme());
            // TODO get color from colors.xml
            background = new ColorDrawable(Color.RED);
            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
            int iconRight = itemView.getRight() - iconMargin;
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
        } else { // view is unSwiped
            background.setBounds(0, 0, 0, 0);
        }

        background.draw(c);
        icon.draw(c);
    }

    public interface ItemTouchHelperTransactionAdapter {
        void onRowMove(int fromPosition, int toPosition);
        void onRowSelected(TransactionListAdapter.TransactionViewHolder myViewHolder);
        void onRowClear(TransactionListAdapter.TransactionViewHolder myViewHolder);
        List<Transaction> getTransactions();
        Transaction getTransactionAt(int adapterPosition);
    }

    // TODO der Name passt nicht so gut, da es ja im Grunde um die MoneyBoxActivity geht
    public interface ItemTouchHelperTransactionActivity {
        Context getContext();
        void onItemDeleteSwipe(Transaction transaction);
        void onItemEditSwipe(Transaction transaction);
        void onListSortChange(List<Transaction> transactions);
    }
}
