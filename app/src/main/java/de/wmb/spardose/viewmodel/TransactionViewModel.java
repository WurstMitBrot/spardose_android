package de.wmb.spardose.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;
import de.wmb.spardose.db.repository.MoneyBoxRepository;
import de.wmb.spardose.db.repository.TransactionRepository;

public class TransactionViewModel extends AndroidViewModel {

    private MoneyBoxRepository moneyBoxRepository;
    private TransactionRepository transactionRepository;
    private LiveData<List<Transaction>> allTransactions;

    public TransactionViewModel(Application application) {
        super(application);
        moneyBoxRepository = new MoneyBoxRepository(application);
        transactionRepository = new TransactionRepository(application);
        allTransactions = transactionRepository.getAllTransactions();
    }

    public LiveData<List<Transaction>> getAllTransactions() {return allTransactions; }

    public LiveData<List<Transaction>> getTransactionByMoneyBoxId(MoneyBox moneyBox){
        return transactionRepository.getTransactionsByMoneyBoxId(moneyBox);
    }

    public void insert(Transaction transaction, MoneyBox moneyBox){
        insert(transaction);
        updateMoneyBoxAmount(moneyBox);
    }

    // shouldn't be used outside this class, the moneyBox has to be updated when inserting Transaction
    private void insert(Transaction transaction) {
        transactionRepository.insert(transaction);
    }

    public void deleteTransactionById(Transaction transaction){
        MoneyBox moneyBox = moneyBoxRepository.getById(transaction.getMoneyBoxID());

        transactionRepository.deleteMoneyBoxById(transaction);

        Double amount = transactionRepository.getAmountByMoneyBoxId(moneyBox);
        moneyBoxRepository.updateAmount(moneyBox, amount);
    }

    public MoneyBox getMoneyBoxById(Long id){
        return moneyBoxRepository.getById(id);
    }

    public void updateTransactionSort(List<Transaction> transactions){
        transactions.forEach(t -> t.setSortPosition(transactions.indexOf(t)));
        transactionRepository.sortCustomTransactions(transactions);
    }

    public void updateTransaction(Transaction transaction, MoneyBox moneyBox) {
        transactionRepository.updateTransaction(transaction);
        updateMoneyBoxAmount(moneyBox);
    }

    private void updateMoneyBoxAmount(MoneyBox moneyBox){
        Double amount = transactionRepository.getAmountByMoneyBoxId(moneyBox);
        moneyBoxRepository.updateAmount(moneyBox, amount);
    }
}
