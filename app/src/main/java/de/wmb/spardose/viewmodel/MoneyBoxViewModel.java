package de.wmb.spardose.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;

import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;
import de.wmb.spardose.db.repository.MoneyBoxRepository;
import de.wmb.spardose.db.repository.TransactionRepository;

public class MoneyBoxViewModel extends AndroidViewModel {

    private MoneyBoxRepository moneyBoxRepository;
    private TransactionRepository transactionRepository;
    private LiveData<List<MoneyBox>> allMoneyBoxes;

    public MoneyBoxViewModel (Application application) {
        super(application);
        moneyBoxRepository = new MoneyBoxRepository(application);
        transactionRepository = new TransactionRepository(application);
        allMoneyBoxes = moneyBoxRepository.getAllMoneyBoxes();
    }

    public LiveData<List<MoneyBox>> getAllMoneyBoxes() {return allMoneyBoxes; }

    public Long insert(MoneyBox moneyBox) {
        Long id = null;
        try {
            id = moneyBoxRepository.insert(moneyBox);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void insert(MoneyBox moneyBox, Transaction transaction){
        Long id;
        id = insert(moneyBox);
        transaction.setMoneyBoxID(id);
        transactionRepository.insert(transaction);

        // add the amount to the MoneyBox, it's the first transaction so we can use it
        MoneyBox freshBox = moneyBoxRepository.getById(id);
        moneyBoxRepository.updateAmount(freshBox, transaction.getAmount());
    }

    public void update(MoneyBox moneyBox){
        moneyBoxRepository.update(moneyBox);
    }

    public void updateMoneyBoxSort(List<MoneyBox> moneyBoxes){
        moneyBoxes.forEach(box -> box.setSortPosition(moneyBoxes.indexOf(box)));
        moneyBoxRepository.sortCustomMoneyBoxes(moneyBoxes);
    }

    public void deleteMoneyBoxById(MoneyBox moneyBox){
        moneyBoxRepository.deleteMoneyBoxById(moneyBox);
    }
}
