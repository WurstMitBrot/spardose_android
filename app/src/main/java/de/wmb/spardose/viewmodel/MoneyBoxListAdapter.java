package de.wmb.spardose.viewmodel;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.wmb.spardose.R;
import de.wmb.spardose.db.entity.MoneyBox;

import static de.wmb.spardose.R.color.negativeAmount;
import static de.wmb.spardose.R.color.positiveAmount;
import static de.wmb.spardose.util.Constants.SORT_TYPE_CUSTOM;
import static de.wmb.spardose.util.HelperMethods.getFormattedAmountString;

public class MoneyBoxListAdapter extends RecyclerView.Adapter<MoneyBoxListAdapter.MoneyBoxViewHolder> implements MoneyBoxItemCallback.ItemTouchHelperMoneyBoxAdapter {

    class MoneyBoxViewHolder extends RecyclerView.ViewHolder{
        private final TextView txtTitle;
        private final TextView txtAmount;
        private View rowView;

        private MoneyBoxViewHolder(View itemView){
            super(itemView);

            rowView = itemView;
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtAmount = itemView.findViewById(R.id.txtAmount);
        }

        public void bind(final MoneyBox moneyBox, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onItemClick(moneyBox);
                    }
                }
            });
        }
    }

    private final LayoutInflater layoutInflater;
    private List<MoneyBox> moneyBoxes;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(MoneyBox moneyBox);
    }

    public void setOnItemListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public MoneyBoxListAdapter(Context context)
    {
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MoneyBoxViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = layoutInflater.inflate(R.layout.moneybox_recyclerview_item, viewGroup, false);
        return new MoneyBoxViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MoneyBoxViewHolder moneyBoxViewHolder, int index) {
        if(moneyBoxes != null){
            MoneyBox current = moneyBoxes.get(index);
            moneyBoxViewHolder.txtTitle.setText(current.getName());
            moneyBoxViewHolder.txtAmount.setText(getFormattedAmountString(current.getAmount()));

            Resources res = moneyBoxViewHolder.itemView.getContext().getResources();
            Resources.Theme theme = moneyBoxViewHolder.itemView.getContext().getTheme();

            // set color of amount text
            if(current.getAmount() < 0){
                moneyBoxViewHolder.txtAmount.setTextColor(res.getColor(negativeAmount, theme));
            }else if(current.getAmount() > 0){
                moneyBoxViewHolder.txtAmount.setTextColor(res.getColor(positiveAmount, theme));
            }

            // binding item click listner
            MoneyBox item = moneyBoxes.get(index);
            moneyBoxViewHolder.bind(item, listener);
        }else{
            moneyBoxViewHolder.txtTitle.setText("Keine Einträge");
        }
    }

    @Override
    public int getItemCount() {
        if(moneyBoxes != null){
            return moneyBoxes.size();
        }else{
            return 0;
        }
    }

    public List<MoneyBox> getMoneyBoxes(){
        return this.moneyBoxes;
    }

    public void setMoneyBoxes(List<MoneyBox> moneyBoxes, int sortType){
        this.moneyBoxes = moneyBoxes;
        sortMoneyBoxes(sortType);
    }

    public void sortMoneyBoxes(int sortType){
        switch (sortType){
            case SORT_TYPE_CUSTOM:
                moneyBoxes.sort((MoneyBox m1, MoneyBox m2) -> m1.getSortPosition().compareTo(m2.getSortPosition()));
                break;
            default:
                moneyBoxes.sort((MoneyBox m1, MoneyBox m2) -> m1.getName().compareTo(m2.getName()));
        }
        notifyDataSetChanged();
    }

    @Override
    public void onRowMove(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onRowSelected(MoneyBoxViewHolder myViewHolder) {
        myViewHolder.rowView.setBackground(new ColorDrawable(Color.GRAY));
    }

    @Override
    public void onRowClear(MoneyBoxViewHolder myViewHolder) {
        myViewHolder.rowView.setBackground(new ColorDrawable(Color.WHITE));
    }

    @Override
    public MoneyBox getMoneyBoxAt(int adapterPosition) {
        return moneyBoxes.get(adapterPosition);
    }
}
