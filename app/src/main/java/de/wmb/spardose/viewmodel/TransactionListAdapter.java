package de.wmb.spardose.viewmodel;

import android.content.Context;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import de.wmb.spardose.R;
import de.wmb.spardose.db.entity.MoneyBox;
import de.wmb.spardose.db.entity.Transaction;

import static de.wmb.spardose.R.color.negativeAmount;
import static de.wmb.spardose.R.color.positiveAmount;
import static de.wmb.spardose.util.Constants.SORT_TYPE_CUSTOM;
import static de.wmb.spardose.util.HelperMethods.getFormattedAmountString;

public class TransactionListAdapter extends RecyclerView.Adapter<TransactionListAdapter.TransactionViewHolder> implements TransactionItemCallback.ItemTouchHelperTransactionAdapter {

    class TransactionViewHolder extends RecyclerView.ViewHolder{
        private final TextView txtDescription;
        private final TextView txtAmount;
        private final TextView txtDate;
        private View rowView;

        private TransactionViewHolder(View itemView){
            super(itemView);
            rowView = itemView;

            txtDescription = itemView.findViewById(R.id.txtDescription);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            txtDate = itemView.findViewById(R.id.txtDate);
        }

        public void bind(final Transaction transaction, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(transaction);
                }
            });
        }
    }

    private final LayoutInflater layoutInflater;
    private List<Transaction> transactions;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Transaction transaction);
    }

    public void setOnItemListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public TransactionListAdapter(Context context)
    {
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = layoutInflater.inflate(R.layout.transaction_recyclerview_item, viewGroup, false);

        return new TransactionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder transactionViewHolder, int index) {
        if(transactions != null){
            Transaction current = transactions.get(index);
            transactionViewHolder.txtDescription.setText(current.getDescription());

            transactionViewHolder.txtAmount.setText(getFormattedAmountString(current.getAmount()));

            Resources res = transactionViewHolder.itemView.getContext().getResources();
            Resources.Theme theme = transactionViewHolder.itemView.getContext().getTheme();

            // set color of amount text
            if(current.getAmount() < 0){
                transactionViewHolder.txtAmount.setTextColor(res.getColor(negativeAmount, theme));
            }else if(current.getAmount() > 0){
                transactionViewHolder.txtAmount.setTextColor(res.getColor(positiveAmount, theme));
            }

            String dateString = current.getPayDate().getDayOfMonth() + "." +
                    current.getPayDate().getMonthValue() + "." +
                    current.getPayDate().getYear();

            transactionViewHolder.txtDate.setText(dateString);
            // binding item click listener
            Transaction item = transactions.get(index);
            transactionViewHolder.bind(item, listener);
        }else{
            transactionViewHolder.txtDescription.setText("Keine Einträge");
        }
    }

    @Override
    public int getItemCount() {
        if(transactions != null){
            return transactions.size();
        }else{
            return 0;
        }
    }

    public void setTransactions(List<Transaction> transactions, int sortType){
        this.transactions = transactions;
        sortTransactions(sortType);
    }

    public void sortTransactions(int sortType){
        switch (sortType){
            case SORT_TYPE_CUSTOM:
                transactions.sort((Transaction t1, Transaction t2) -> t1.getSortPosition().compareTo(t2.getSortPosition()));
                break;
            default:
                //transactions.sort((Transaction t1, Transaction t2) -> t1.getPayDate().compareTo(t2.getPayDate()));
        }
        notifyDataSetChanged();
    }

    @Override
    public void onRowMove(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onRowSelected(TransactionViewHolder myViewHolder) {
        myViewHolder.rowView.setBackground(new ColorDrawable(Color.GRAY));
    }

    @Override
    public void onRowClear(TransactionViewHolder myViewHolder) {
        myViewHolder.rowView.setBackground(new ColorDrawable(Color.WHITE));
    }

    @Override
    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public Transaction getTransactionAt(int adapterPosition) {
        return transactions.get(adapterPosition);
    }
}
