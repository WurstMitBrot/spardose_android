package de.wmb.spardose.viewmodel;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.util.Collections;
import java.util.List;

import de.wmb.spardose.R;
import de.wmb.spardose.db.entity.MoneyBox;

public class MoneyBoxItemCallback extends ItemTouchHelper.SimpleCallback {

    // TODO prüfen ob das nicht auch mit generischen Typen geht, wäre eine gute Übung
    private ItemTouchHelperMoneyBoxActivity activity;
    private final ItemTouchHelperMoneyBoxAdapter adapter;

    private boolean hasMoved = false;
    private List<MoneyBox> moneyBoxes = null;
    private int startPosition = -1;
    private int endPosition = -1;

    public MoneyBoxItemCallback(ItemTouchHelperMoneyBoxActivity activity, ItemTouchHelperMoneyBoxAdapter adapter, int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);
        this.activity = activity;
        this.adapter = adapter;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        // variable to swap the element while moving
        Integer fromPos = viewHolder.getAdapterPosition();
        endPosition = target.getAdapterPosition();

        // only save from pos during the first call of onMove
        if(!hasMoved){
            hasMoved = true;
            startPosition = fromPos;
            moneyBoxes = adapter.getMoneyBoxes();
        }

        // move item in recyclerview and swap in list
        adapter.onRowMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        Collections.swap(moneyBoxes, fromPos, endPosition);

        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder,
                                  int actionState) {
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            // TODO warum nicht aufs Inteface abfragen?
            // change background to gray
            if (viewHolder instanceof MoneyBoxListAdapter.MoneyBoxViewHolder) {
                MoneyBoxListAdapter.MoneyBoxViewHolder moneyBoxViewHolder =
                        (MoneyBoxListAdapter.MoneyBoxViewHolder) viewHolder;
                adapter.onRowSelected(moneyBoxViewHolder);
            }
        }
        super.onSelectedChanged(viewHolder, actionState);
    }

    @Override
    public void clearView(RecyclerView recyclerView,
                          RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);

        if(hasMoved && startPosition != endPosition){
            // write swapped list to database
            activity.onListSortChange(moneyBoxes);

            hasMoved = false;
            startPosition = -1;
            endPosition = -1;
        }

        // TODO warum nicht aufs Inteface abfragen?
        // turn background back to normal
        if (viewHolder instanceof MoneyBoxListAdapter.MoneyBoxViewHolder) {
            MoneyBoxListAdapter.MoneyBoxViewHolder moneyBoxViewHolder =
                    (MoneyBoxListAdapter.MoneyBoxViewHolder) viewHolder;
            adapter.onRowClear(moneyBoxViewHolder);
        }
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        MoneyBox moneyBox = adapter.getMoneyBoxAt(viewHolder.getAdapterPosition());
        if (i == ItemTouchHelper.LEFT) {
            activity.onItemDeleteSwipe(moneyBox);
        } else if (i == ItemTouchHelper.RIGHT) {
            activity.onItemEditSwipe(moneyBox);
        }
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

        dX = dX/ 2;
        Drawable icon = activity.getContext().getResources().getDrawable(R.drawable.ic_edit_white_24dp, activity.getContext().getTheme());
        ColorDrawable background;
        background = new ColorDrawable(Color.GREEN);

        View itemView = viewHolder.itemView;
        int backgroundCornerOffset = 500; //so background is behind the rounded corners of itemView

        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 4;
        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
        int iconBottom = iconTop + icon.getIntrinsicHeight();

        if (dX > 0) { // Swiping to the right
            icon = activity.getContext().getResources().getDrawable(R.drawable.ic_edit_white_24dp, activity.getContext().getTheme());
            // TODO get color from colors.xml
            background = new ColorDrawable(Color.BLUE);
            int iconRight = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
            int iconLeft = itemView.getLeft() + iconMargin;
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
            background.setBounds(itemView.getLeft(), itemView.getTop(),
                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
        } else if (dX < 0) { // Swiping to the left
            icon = activity.getContext().getResources().getDrawable(R.drawable.ic_delete_white, activity.getContext().getTheme());
            // TODO get color from colors.xml
            background = new ColorDrawable(Color.RED);
            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
            int iconRight = itemView.getRight() - iconMargin;
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
        } else { // view is unSwiped
            background.setBounds(0, 0, 0, 0);
        }

        background.draw(c);
        icon.draw(c);
    }

    public interface ItemTouchHelperMoneyBoxAdapter {
        void onRowMove(int fromPosition, int toPosition);
        void onRowSelected(MoneyBoxListAdapter.MoneyBoxViewHolder myViewHolder);
        void onRowClear(MoneyBoxListAdapter.MoneyBoxViewHolder myViewHolder);
        List<MoneyBox> getMoneyBoxes();
        MoneyBox getMoneyBoxAt(int adapterPosition);
    }

    // TODO der Name passt nicht so gut, da es ja im Grunde um die MainActivity geht
    public interface ItemTouchHelperMoneyBoxActivity {
        Context getContext();
        void onItemDeleteSwipe(MoneyBox moneyBox);
        void onItemEditSwipe(MoneyBox moneyBox);
        void onListSortChange(List<MoneyBox> moneyBoxes);
    }
}
